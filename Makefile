include .mk/GNUmakefile

.DEFAULT_GOAL = kube-help

VAGRANT_STATUS   ?= $(shell vagrant status --machine-readable | grep state-human-short | awk -F',' '{print $$2 ": " $$4}')
VAGRANT_STARTED  ?= $(shell echo $(VAGRANT_STATUS) | grep running > /dev/null; echo $$?)
MINIKUBE_STATUS  ?= $(shell minikube status | grep minikubeVM)
MINIKUBE_STARTED ?= $(shell echo $(MINIKUBE_STATUS) | grep Running > /dev/null; echo $$?)
LOCALKUBE_STATUS ?= $(shell minikube status | grep localkube)
BROWSER_PATH     ?= $(shell which xdg-open || which gnome-open)
AEGIR_URL        ?= $(shell vagrant ssh -c "sudo -u aegir -H drush @hm uli" 2> /dev/null)

AEGIR_SSH_PUB_KEY ?= $(shell vagrant ssh -c"sudo cat /var/aegir/.ssh/id_rsa.pub" 2> /dev/null)
AEGIR_HOME        ?= /var/aegir
AEGIR_SSH_DIR     ?= $(AEGIR_HOME)/.ssh
MINIKUBE_IP       ?= $(shell minikube ip)
ANSIBLE_PATH      ?= /vagrant/tests/ansible
KUBECTL_RELEASE   ?= v1.3.0
KUBECTL_CURRENT   ?= $(shell minikube ssh "kubectl version 2> /dev/null| grep Client | grep $(KUBECTL_RELEASE) > /dev/null && echo 0 || echo 1; exit 0")
KUBECTL_PATH      ?= /usr/local/bin/kubectl


kube-help:
	@echo "The following 'make' targets are available: (usage 'make <target>')"
	@echo "  kube-help"
	@echo "    Print this help message."
	@echo "  kube-status"
	@echo "    Print a brief status of the various VMs."
	@echo "  kube-dashboards"
	@echo "    Open both Kubernetes and Aegir dashboard in a browser."
	@echo "  kube-start"
	@echo "    Launch VMs and run initial configuration."
	@echo "  kube-stop"
	@echo "    Stop the currently running VMs."
	@echo "  kube-destroy"
	@echo "    Print this help message"
	@echo "  kube-rebuild"
	@echo "    Print this help message"

kube-status:
	@echo $(MINIKUBE_STATUS)
	@echo $(LOCALKUBE_STATUS)
	@echo $(VAGRANT_STATUS)

kube-dashboards:
	@exec $(BROWSER_PATH) $(AEGIR_URL)
	@minikube dashboard

kube-start: start-vms aegir-user-on-minikube minikube-hosts-on-aegir kubectl-on-minikube

kube-stop:
	@vagrant halt
	@minikube stop

kube-destroy:
	@vagrant destroy -f
	@minikube delete

kube-rebuild: kube-destroy kube-start

start-vms: minikube
	@if [[ $(VAGRANT_STARTED) -eq 0 ]]; then echo 'vagrant already started'; else vagrant up; fi
	@if [[ $(MINIKUBE_STARTED) -eq 0 ]]; then echo 'minikube already started'; else minikube start; fi

aegir-user-on-minikube:
	@minikube ssh "(id -u aegir > /dev/null && echo 'aegir user exists') || (sudo adduser --home $(AEGIR_HOME) --disabled-password aegir && sudo passwd aegir -d > /dev/null && echo 'created aegir user'); exit 0" 2> /dev/null
	@minikube ssh "sudo adduser aegir docker && echo 'added aegir user to docker group'"
	@minikube ssh "if [[ -d $(AEGIR_SSH_DIR) ]]; then echo 'aegir ssh directory exists.'; else sudo mkdir -p $(AEGIR_SSH_DIR) && echo 'created aegir ssh directory'; fi"
	@minikube ssh "sudo su -c \"echo '$(AEGIR_SSH_PUB_KEY)' > $(AEGIR_SSH_DIR)/authorized_keys \" && echo 'added aegir ssh pub key'"
	@minikube ssh "sudo chown -R aegir:aegir $(AEGIR_HOME) && echo 'fixed ownership of aegir ssh directory'"

minikube-hosts-on-aegir:
	@vagrant ssh -c"sudo ansible-playbook -i $(ANSIBLE_PATH)/hosts $(ANSIBLE_PATH)/minikube_hosts.yml -e\"minikube_ip=$(MINIKUBE_IP)\""

kubectl-on-minikube:
	@if [[ $(KUBECTL_CURRENT) -eq 0 ]]; then echo 'kubectl on minikube is current'; else echo 'installing kubectl on minikube';  minikube ssh "sudo curl -sSLo $(KUBECTL_PATH) https://storage.googleapis.com/kubernetes-release/release/$(KUBECTL_RELEASE)/bin/linux/amd64/kubectl"; echo 'installed kubectl on minikube'; fi
	@minikube ssh "sudo chmod a+x $(KUBECTL_PATH)"


