<?php

/**
 * A basic configuration file class.
 *
 * Just a file containing the value passed to us.
 */
class Provision_Config_container_kubernetes extends Provision_Config_container {
  /**
   * Template file to load. In the same directory as this class definition.
   */
  public $template = 'kubernetes.tpl.php';

  /**
   * Where the file generated will end up.
   */
  function filename() {
    return $this->kubernetes_config_path . '/kubernetes.conf';
  }

}
