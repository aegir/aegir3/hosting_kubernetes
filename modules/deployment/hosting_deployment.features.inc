<?php
/**
 * @file
 * hosting_deployment.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hosting_deployment_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hosting_deployment_node_info() {
  $items = array(
    'deployment' => array(
      'name' => t('Deployment'),
      'base' => 'node_content',
      'description' => t('An deployed application instance via a container service.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
