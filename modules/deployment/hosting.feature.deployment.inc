<?php

/**
 * @file
 * Expose the Aegir Deployment feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_deployment_hosting_feature() {
  $features['deployment'] = array(
    'title' => t('Deployments'),
    'description' => t('Provides a deployment content-type.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_deployment',
    #'enable' => 'hosting_deployment_feature_enable_callback',
    #'disable' => 'hosting_deployment_feature_disable_callback',
    // associate with a specific node type.
    'node' => 'deployment',
    'group' => 'experimental'
    );
  return $features;
}
