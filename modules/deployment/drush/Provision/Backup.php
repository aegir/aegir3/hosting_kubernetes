<?php

/**
 * Generates backups for Aegir.
 */
abstract class Provision_Backup {

  protected $name;
  protected $items;

  public function __construct($name) {
    $this->name = $name;
    $this->items = array();
  }

  public function addItemToBackup($item) {
    drush_log('Adding volume to backup: ' . $item . '...');
    $this->items[] = $item;
  }

  /**
   * Save the contents of the backup items to an archive.
   *
   * @see drush_provision_drupal_provision_backup()
   */
  abstract public function save();

  protected function getValidBackupItems() {
    $items_to_backup = array();

    foreach ($this->items as $item) {
      if (file_exists($item)) {
        $items_to_backup[] = $item;
      }
      else {
        drush_log('Volume ' . htmlspecialchars($item, ENT_QUOTES, 'UTF-8') . ' does not exist.  Skipping...');
      }
    }
    return $items_to_backup;
  }

}
