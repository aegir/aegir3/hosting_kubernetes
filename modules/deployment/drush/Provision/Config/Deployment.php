<?php

/**
 * A configuration class to write a deployment's resource config files.
 */
class Provision_Config_Deployment extends Provision_Config_Resource {

  public $template = FALSE;

  /**
   * Override the parent class, as we don't use a static template, and are
   * going to write multiple resource config files.
   */
  public function write() {
    // @todo We're currently fetching resource templates directly from the
    // application context. This makes the application config class, and the
    // template files it generates redundant. We need to decide whether to use
    // the template files, in which case we alter this method, or remove the
    // application config stuff.
    foreach ($this->getResources() as $id => $resource) {
      $path = $this->getConfigPath($id, $resource['filename']);
      $contents = $this->unpackYAML($resource['template']);
      provision_file()->file_put_contents($path, $contents)
        ->succeed('Wrote application config file template to @path')
        ->fail('Failed to write application config file template to @path');
    }
    // Sync out to the server.
    // @todo Check whether perms aren't changeable because of minikube.
    $this->getContainerServer()->sync($this->getResourceDir(), array('no-perms' => TRUE));
  }

  function getContext($type = 'deployment') {
    return parent::getContext($type);
  }

  function getResources() {
    $resources = $this->getApplicationResources();
    $replacements = $this->getReplacements();
    foreach ($resources as $id => $resource) {
      $tokens = array_keys($replacements[$id]);
      $values = array_values($replacements[$id]);
      $resources[$id]['template'] = str_replace($tokens, $values, $resource['template']);
    }
    return $resources;
  }

  function getReplacements() {
    return $this->getContext()->replacements;
  }

  function getApplicationResources() {
    $application_config_class = $this->getApplicationConfigClass();
    $application_config = new $application_config_class($this->getApplication()->name);
    return $application_config->getResources();
  }

  function getApplicationConfigClass() {
    return 'Provision_Config_Application';
  }

  function getApplication() {
    return $this->getContext()->application;
  }

}
