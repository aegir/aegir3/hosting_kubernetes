<?php

/**
 * @file
 * Implement drush hooks for the deployment module.
 */

/**
 * Implements hook_hosting_TASK_OBJECT_context_options().
 */
function hosting_deployment_hosting_deployment_context_options(&$task) {
  $task->context_options['server'] = '@server_master';
  $application = field_get_items('node', $task->ref, 'field_deployment_application');
  $task->context_options['application'] = hosting_context_name($application[0]['target_id']);
  $task->context_options['replacements'] = hosting_deployment_get_replacements($task->ref);
}

/**
 * Helper function to look up a deployment's application.
 *
 * @todo move this to a deployment entity controller.
 */
function hosting_deployment_get_application($node) {
  return node_load($node->field_deployment_application['und'][0]['target_id']);
}

/**
 * Helper function to look up a deployment's tokens.
 *
 * @todo move this to a deployment entity controller.
 */
function hosting_deployment_get_replacements($node) {
  $resources = hosting_deployment_get_resources($node);
  $application = hosting_deployment_get_application($node);
  $deployment_replacements = [];
  foreach ($resources as $id => $resource) {
    $resource_entity = entity_load('resource', [$id]);
    $resource_entity = $resource_entity[$id];
    $data = [
      'deployment' => $node,
      'application' => $application,
      'resource' => $resource_entity,
    ];
    $template_tokens = token_scan($resource['template']);
    $replacements = [];
    foreach ($template_tokens as $type => $tokens) {
      $options = isset($resource['parent_id']) ? ['parent_id' => $resource['parent_id']] : [];
      $replacements += token_generate($type, $tokens, $data, $options);
    }
    $deployment_replacements[$id] = $replacements;
  }
  return $deployment_replacements;
}

/**
 * Return a list of resources appropriate for inclusion in a deployment
 * context.
 *
 * @todo move this to a deployment entity controller.
 */
function hosting_deployment_get_resources($node) {
  $application = hosting_deployment_get_application($node);
  return hosting_application_get_resources($application);
}

/**
 * Implements hook_drush_context_import().
 */
function hosting_deployment_drush_context_import($context, &$node) {
  if ($context->type == 'deployment') {
    if (!isset($node->title)) {
      $node->title = str_replace('deployment_', '', trim($context->name, '@'));
    }
    $node->field_deployment_application['und'][0]['target_id'] = hosting_context_nid($context->application);
  }
}

