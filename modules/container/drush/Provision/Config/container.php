<?php

/**
 * A basic configuration file class.
 *
 * Just a file containing the value passed to us.
 */
class Provision_Config_container extends Provision_Config {
  /**
   * Override the write method.
   */
  function write() {
    parent::write();

    // Sync the config to a remote server if necessary.
    $this->data['server']->sync($this->filename());
  }

  /**
   * Override the unlink method.
   */
  function unlink() {
    parent::unlink();

    // Remove the config from a remote server if necessary.
    $this->data['server']->sync($this->filename());
  }
}
