<?php

/**
 * @file
 * Container service implementation for the hosting front end.
 */

/**
 * The base service type class , registered with hook_hosting_service_type().
 */
class hostingService_container extends hostingService {
  /**
   * the value stored in the service column of hosting_service table.
   */
  public $service = 'container';
}
