<?php

/**
 * @file Provision named context application class.
 */


/**
 * Class for the application context.
 */
class Provision_Context_application extends Provision_Context {
  public $parent_key = 'container_server';

  static function option_documentation() {
    return array(
      'server' => 'application: drush backend server; default @server_master',
      'container_server' => 'application: container server hosting the application.',
      'resources' => 'application: a list of resources definitions that make up this application',
    );
  }

  function init_application() {
    $this->setProperty('server', '');
    $this->setProperty('container_server', '');
    $this->setProperty('resources', array());
  }
}

