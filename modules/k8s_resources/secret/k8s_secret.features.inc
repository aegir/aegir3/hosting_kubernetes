<?php
/**
 * @file
 * k8s_secret.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_secret_eck_bundle_info() {
  $items = array(
    'resource_k8s_secret' => array(
      'machine_name' => 'resource_k8s_secret',
      'entity_type' => 'resource',
      'name' => 'k8s_secret',
      'label' => 'Secret',
      'config' => array(
        'managed_properties' => array(
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
          'title' => 0,
        ),
        'template' => array(
          'resource' => 'apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  password: MWYyZDFlMmU2N2Rm
  username: YWRtaW4=
',
        ),
        'replacements' => array(),
      ),
    ),
  );
  return $items;
}
