<?php
/**
 * @file
 * k8s_persistent_volume.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function k8s_persistent_volume_eck_bundle_info() {
  $items = array(
    'resource_k8s_persistent_volume' => array(
      'machine_name' => 'resource_k8s_persistent_volume',
      'entity_type' => 'resource',
      'name' => 'k8s_persistent_volume',
      'label' => 'Persistent Volume',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'uid' => 0,
          'created' => 0,
          'changed' => 0,
        ),
        'template' => array(
          'resource' => '---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: local-pv-1
  labels:
    type: local
spec:
  capacity:
    storage: 40Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: /tmp/pv-1
',
        ),
        'replacements' => array(
          0 => array(
            'selector' => 'spec/capacity/storage',
            'token' => '[resource:field-storage-capacity]Gi',
          ),
          1 => array(
            'selector' => 'spec/hostPath/path',
            'token' => '/tmp/[application:name]/[deployment:name]',
          ),
        ),
      ),
    ),
  );
  return $items;
}
