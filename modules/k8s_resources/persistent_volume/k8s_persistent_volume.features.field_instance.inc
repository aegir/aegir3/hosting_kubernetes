<?php
/**
 * @file
 * k8s_persistent_volume.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function k8s_persistent_volume_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'resource-k8s_persistent_volume-field_storage_capacity'.
  $field_instances['resource-k8s_persistent_volume-field_storage_capacity'] = array(
    'bundle' => 'k8s_persistent_volume',
    'default_value' => array(
      0 => array(
        'value' => 20,
      ),
    ),
    'deleted' => 0,
    'description' => 'Define the amount of storage available to this persistent volume.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_storage_capacity',
    'label' => 'Storage capacity',
    'required' => 1,
    'settings' => array(
      'max' => 50,
      'min' => 1,
      'prefix' => '',
      'suffix' => 'Gi',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Define the amount of storage available to this persistent volume.');
  t('Storage capacity');

  return $field_instances;
}
