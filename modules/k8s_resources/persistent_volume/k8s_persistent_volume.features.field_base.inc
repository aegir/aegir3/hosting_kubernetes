<?php
/**
 * @file
 * k8s_persistent_volume.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function k8s_persistent_volume_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_k8s_persistent_volume'.
  $field_bases['field_k8s_persistent_volume'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_k8s_persistent_volume',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'behaviors' => array(),
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'k8s_persistent_volume' => 'k8s_persistent_volume',
        ),
      ),
      'target_type' => 'resource',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_storage_capacity'.
  $field_bases['field_storage_capacity'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_storage_capacity',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
