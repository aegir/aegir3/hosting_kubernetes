<?php
/**
 * @file
 * k8s_mysql_deployment.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function k8s_mysql_deployment_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'resource-k8s_mysql_deployment-field_k8s_container_image'.
  $field_instances['resource-k8s_mysql_deployment-field_k8s_container_image'] = array(
    'bundle' => 'k8s_mysql_deployment',
    'default_value' => array(
      0 => array(
        'value' => 'mysql:5.6',
      ),
    ),
    'deleted' => 0,
    'description' => 'Choose the container image to use for this MySQL deployment.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_container_image',
    'label' => 'Container image',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'resource-k8s_mysql_deployment-field_k8s_mysql_service'.
  $field_instances['resource-k8s_mysql_deployment-field_k8s_mysql_service'] = array(
    'bundle' => 'k8s_mysql_deployment',
    'deleted' => 0,
    'description' => 'MySQL Deployments require a service to expose its port to other containers.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_mysql_service',
    'label' => 'MySQL service',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'resource-k8s_mysql_deployment-field_k8s_pv_claim'.
  $field_instances['resource-k8s_mysql_deployment-field_k8s_pv_claim'] = array(
    'bundle' => 'k8s_mysql_deployment',
    'deleted' => 0,
    'description' => 'MySQL deployments require a persistent volume claim on which to store database files.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_pv_claim',
    'label' => 'Persistent volume claim',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'resource-k8s_mysql_deployment-field_k8s_secret'.
  $field_instances['resource-k8s_mysql_deployment-field_k8s_secret'] = array(
    'bundle' => 'k8s_mysql_deployment',
    'deleted' => 0,
    'description' => 'MySQL deployments require secret credentials (username and password) in order to secure access to its services.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'resource',
    'field_name' => 'field_k8s_secret',
    'label' => 'Secret',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'inline_entity_form',
      'settings' => array(
        'fields' => array(),
        'type_settings' => array(
          'allow_clone' => 0,
          'allow_existing' => FALSE,
          'allow_new' => 1,
          'delete_references' => 1,
          'label_plural' => 'entities',
          'label_singular' => 'entity',
          'match_operator' => 'CONTAINS',
          'override_labels' => 0,
        ),
      ),
      'type' => 'inline_entity_form_single',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choose the container image to use for this MySQL deployment.');
  t('Container image');
  t('MySQL Deployments require a service to expose its port to other containers.');
  t('MySQL deployments require a persistent volume claim on which to store database files.');
  t('MySQL deployments require secret credentials (username and password) in order to secure access to its services.');
  t('MySQL service');
  t('Persistent volume claim');
  t('Secret');

  return $field_instances;
}
