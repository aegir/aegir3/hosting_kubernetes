<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

/**
 * Provides a custom entity class representing configurable resources.
 */
class ResourceEntity extends Entity {

  function getFilename() {

    return $this->type . '.yaml';

  }

  function getTokenizedTemplateYAML(int $depth) {

    return Yaml::dump(Yaml::parse($this->getTokenizedTemplate()), $depth);

  }

  function getTokenizedTemplate() {

    $template = $this->getTemplate();
    $replacements = $this->getReplacements();

    $tokenized = ResourceEntityBundle::tokenize($template, $replacements);

    return $tokenized;

  }

  function getTemplate() {

    $bundle = $this->getBundle();

    return $bundle->config['template']['resource'];

  }

  function getReplacements() {

    $bundle = $this->getBundle();

    return $bundle->config['replacements'];

  }

  function getBundle() {

    return bundle_load('resource', $this->type);

  }

}
