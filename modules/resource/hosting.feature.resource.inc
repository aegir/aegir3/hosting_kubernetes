<?php

/**
 * @file
 * Expose the Aegir resource feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_resource_hosting_feature() {
  $features['resource'] = array(
    'title' => t('Resource'),
    'description' => t('Provides a Resource entity-type.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_resource',
    'group' => 'experimental'
    );
  return $features;
}
