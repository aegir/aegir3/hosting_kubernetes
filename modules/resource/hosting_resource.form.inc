<?php
/**
 * @file
 * Form hooks, handlers and callbacks.
 */

/**
 * Implements hook_form_alter().
 */
function hosting_resource_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'eck__bundle__edit_form') {
    $resource_bundle = new ResourceEntityBundle($form, $form_state);
    return $resource_bundle->alterEditForm();
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function hosting_resource_inline_entity_form_entity_form_alter(&$entity_form, &$form_state) {
  if ($entity_form['#entity_type'] == 'resource') {
    // Hide the resource title, as it's set in the submit handler.
    $entity_form['title']['#required'] = FALSE;
    $entity_form['title']['#access'] = FALSE;
    // Prepend our submit handler that will populate the entity tiele/name field.
    $entity_form['#element_validate'] = array_merge(['hosting_resource_inline_entity_form_validate'], $entity_form['#element_validate']);
    // Prepend our submit handler that will populate the entity tiele/name field.
    $entity_form['#ief_element_submit'] = array_merge(['hosting_resource_inline_entity_form_submit'], $entity_form['#ief_element_submit']);
  }
}

/**
 * Validation handler for resource entity forms.
 */
function hosting_resource_inline_entity_form_validate($form, &$form_state) {
  #$resource_bundle = new ResourceEntityBundle($form, $form_state);
  #return $resource_bundle->submitEditForm();
  if (empty($form_state['input']['title'])) {
    // @todo Figure out if form_set_error() can set an error on the application
    // form from this element validation.
    return drupal_set_message(t('Please set a title prior to adding resources.'), 'error');
  }
}

/**
 * Submit handler for resource entity forms.
 */
function hosting_resource_inline_entity_form_submit($form, &$form_state) {
  #$resource_bundle = new ResourceEntityBundle($form, $form_state);
  #return $resource_bundle->submitEditForm();
  $application_name = $form_state['input']['title'];
  if (!empty($application_name)) {
    $values =& $form_state['values'];
    $parents = $form['#array_parents'];
    $parents[] = 'title';
    drupal_array_set_nested_value($values, $parents, $application_name);
  }
}

/**
 * Submit handler for the ECK edit form.
 */
function hosting_resource_eck_edit_form_submit($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->submitEditForm();
}

/**
 * Validation handler for the ECK edit form.
 */
function hosting_resource_eck_edit_form_validate($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->validateEditForm();
}

/**
 * Resource template file upload AJAX submit handler.
 */
function hosting_resource_ajax_upload_submit($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->submitUpload();
}

/**
 * Resource template file upload AJAX callback.
 */
function hosting_resource_ajax_upload_callback($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->getUploadFormElements();
}

/**
 * Replacement removal AJAX submit handler.
 */
function hosting_resource_ajax_replacement_remove($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->removeReplacement();
}

/**
 * Replacement addition AJAX submit handler.
 */
function hosting_resource_ajax_replacement_add($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->addReplacement();
}

/**
 * Replacement addition/removal AJAX callback.
 */
function hosting_resource_ajax_replacement_callback($form, &$form_state) {
  $resource_bundle = new ResourceEntityBundle($form, $form_state);
  return $resource_bundle->getReplacementFormElements();
}

